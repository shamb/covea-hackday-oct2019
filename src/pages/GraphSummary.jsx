import React from "react";

import DataProviderContext from '../DataProviderContext';

function GraphSummary(props) {

  return (
      <DataProviderContext.Consumer>
        { contextData =>
          <>
            <h2>GraphSummary</h2>
            <p>Pi chart here, based on this data...</p>
            <pre>{JSON.stringify(contextData.testEntries, null, 2)}</pre>
          </>
        }
      </DataProviderContext.Consumer>
  );
}

export default GraphSummary;
