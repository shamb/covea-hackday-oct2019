import React from "react";

import DataProviderContext from '../DataProviderContext';

function TestSummary(props) {

  return (
    <DataProviderContext.Consumer>
      { contextData => 
        <div id="testResults" className="tabcontent active">
          <table>
            <thead>
              {/* Test case name - {testEntries[index].name}*/}
              <th>Test name</th>
              {/* Number of tests in the suite where all steps executed successfully */}
              <th>Passed</th>
              {/* Number of tests in the suite where there are any step execution failures */}
              <th>Failed</th>
              <th></th>
            </thead>
            <tr>
              <td>AT-54-AC-7 - TextInput field Change placeholder</td>

              <td>10</td>
              <td>0</td>
              <td><a href="./test.html">Show more</a></td>
            </tr>
            <tr>
              <td>
                AT-54-AC-1-AC-8 - Edit TextInput field
              </td>
              <td>5</td>
              <td>7</td>
              <td><a href="./test.html">Show more</a></td>
            </tr>
          </table>
          <br/><br/>
          <pre>{JSON.stringify(contextData.testEntries, null, 2)}</pre>
        </div>
      }
    </DataProviderContext.Consumer>
  );

}

export default TestSummary;
