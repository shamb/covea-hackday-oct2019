import React from 'react';

const DataProviderContext = React.createContext();

export default DataProviderContext;