import React, { useState } from "react";

import GraphSummary from '../pages/GraphSummary';
import TestSummary from '../pages/TestSummary';

const TEST_SUMMARY = "testSummary";
const GRAPH_SUMMARY = "graphSummary";

function MainApp(props) {
  const [page, setPage] = useState(TEST_SUMMARY);

  const openTab = (tab) => {
    setPage(tab);
  }

  const showErrorPage = page!==TEST_SUMMARY && page!==GRAPH_SUMMARY;

  return (
    <div className="container">

      {/* Menu bar */}
      <div className="tab">
        <button className="tablinks" disabled={page===TEST_SUMMARY} onClick={() => openTab(TEST_SUMMARY)}>Test summary</button>
        <button className="tablinks" disabled={page===GRAPH_SUMMARY} onClick={() => openTab(GRAPH_SUMMARY)}>Graph summary</button>
      </div>

      {/* Page */}
      {page===TEST_SUMMARY && <TestSummary/>}
      {page===GRAPH_SUMMARY && <GraphSummary/>}
      {showErrorPage && <p>Whoops, something went wrong!</p>}

    </div>
  );
}


export default MainApp;
