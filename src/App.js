import React, { useEffect, useState } from "react";

import MainApp from './main/MainApp';
import DataProviderContext from './DataProviderContext';

import "./App.css";

function App() {
  const [testEntries, setTestEntries] = useState(null);
  const [appIsReady, setAppIsReady] = useState(false);
  let content;

  useEffect(() => {
    // component mount
    fetch("/data/sample.json")
      .then(response => response.json())
      .then(data => onReady(data));
  }, []);

  const onReady = data => {
    // Runs when we load the data.
    setTestEntries(data);
    setAppIsReady(true);
  };

  if (!appIsReady) {
    // View to render when we don't have data yet.
    content = <h2>Loading...</h2>;
  } else {
    // View to render when we do have data.
    const contextData = { testEntries };
    content = (
      <div className="container">
        <DataProviderContext.Provider value={contextData} >
          <MainApp dataProvider={testEntries} />
        </DataProviderContext.Provider>
      </div>
    );
  }
  return <div className="App">{content}</div>;
}

export default App;
